#!/bin/bash




#install: configuration-wp-cli

#configuration-wp-cli:

#	@echo "Configuration des paramètres de wordpress..."
	wp core config --allow-root --dbname=${DB_NAME}	--dbuser=${DB_USER} --dbhost=${DB_HOST} --dbpass=${DB_PWD} 

	wp core install --allow-root \
			--url=${WP_URL} \
			--title=${WP_TITLE} \
			--admin_user=${WP_ADMIN_USER} \
			--admin_password=${WP_ADMIN_PWD} \
			--admin_email=${WP_ADMIN_EMAIL}

	wp option update siteurl ${WP_URL}
