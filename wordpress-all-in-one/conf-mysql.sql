SET PASSWORD FOR 'root'@'localhost' = PASSWORD('');

CREATE DATABASE wordpress;

GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpressuser'@'localhost' IDENTIFIED BY 'password' ;

-- DROP DATABASE test;

